docker-java-s3
==============
Creates a Docker image containing the latest Java SDK and a version of s3cmd.  The image also contains default configuration to talk to DreamObjects rather than Amazon S3 (can be overridden via a Docker VOLUME if needed.)

Usage
=====
To do anything useful, you need to supply the access key and secret key for your account, e.g.

    s3cmd --access_key=<ACCESS_KEY>--secret_key=<SECRET_KEY> la